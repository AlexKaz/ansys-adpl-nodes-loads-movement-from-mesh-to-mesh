Movement node forces from shell to solid mesh at any cutting plane. 
There are both ADPL and Scilab code.

If this code is usuful for you donate please, https://paypal.me/alvlk , https://yoomoney.ru/to/410019721796762

date june-july 2017, Perm, Russia
AlexKaz, Kazancev Alexander, alexkazancev@bk.ru

Примитивным способом перикидывал узловые силы с оболочечной сетки на твёрдотельную
в произвольном сечении балки. Здесь какая-то промежуточная версия кода.
Если кидать с твёрдотельной сетки на твёрдотельную - аппроксимация работает. 
Отчёт с результатами нигде не опубликован, презентация с основными моментами тоже пока не особо афишируется. 
Код содержит как ADPL-скрипты, так и Scilab-обработчик.
